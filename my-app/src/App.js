
import './App.css';
import React,{Component}from 'react';
import { CardList } from './component/card-list/card-list';
import {SearchBox} from './component/search-box/search-box';


class App extends Component {
  
  constructor(){
    super();
   this.state = {
    Monster : [],
    searchField: ''
   };
  }
  componentDidMount(){
    fetch("https://jsonplaceholder.typicode.com/users")
    .then(response => response.json())
    .then(users => this.setState({Monster : users}))
  }

  handleChange = (e)=>{
    this.setState({searchField : e.target.value})
    console.log(e)
  }
  
  
render() { 

  const{Monster, searchField} = this.state;
  const filteredmonster = Monster.filter(monster => monster.name.toLowerCase().includes(searchField.toLowerCase()));
    return (
      <div className="App">
        
        <h1>Monster Rolodex</h1>
        
        <SearchBox 
            monster={Monster} 
            handleChange={this.handleChange}
            placeholder="search monster">
        </SearchBox>
       
        <CardList monster={filteredmonster}></CardList>
    </div>
    ) 
  }
}
export default App; 
